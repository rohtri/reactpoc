import React, { useState } from 'react';
import { AppLoading } from 'expo';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import { Provider as PaperProvider, DefaultTheme } from 'react-native-paper'
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';


import * as Font from 'expo-font';
import AppNavigator from './navigation/AppNavigator';

// function HomeScreen() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Home!</Text>
//     </View>
//   );
// }

// function SettingsScreen() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Settings!</Text>
//     </View>
//   );
// }

// const Tab = createMaterialBottomTabNavigator();


// export default function App() {
//   return (
//     <NavigationContainer>
//       <Tab.Navigator>
//         <Tab.Screen name="Home" component={HomeScreen} />
//         <Tab.Screen name="Settings" component={SettingsScreen} />
//       </Tab.Navigator>
//     </NavigationContainer>
//   );
// }

export default function App(props) {

  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <PaperProvider >
          <AppNavigator/>
        </PaperProvider>
      </View>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      'Material Icons': require('@expo/vector-icons/build/vendor/react-native-vector-icons/Fonts/MaterialIcons.ttf'),
      'Merck-font' : require('./assets/Merck.otf'),
      ...Ionicons.font,
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});


// import * as React from 'react';
// import { Text, View } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
// import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
// import { MaterialCommunityIcons } from '@expo/vector-icons';

// function Feed() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Feed!</Text>
//     </View>
//   );
// }

// function Profile() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Profile!</Text>
//     </View>
//   );
// }

// function Notifications() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Notifications!</Text>
//     </View>
//   );
// }

// const Tab = createMaterialBottomTabNavigator();

// function MyTabs() {
//   return (
//     <Tab.Navigator
//       initialRouteName="Feed"
//       activeColor="#e91e63"
//       labelStyle={{ fontSize: 12 }}
//       style={{ backgroundColor: 'tomato' }}
//     >
//       <Tab.Screen
//         name="Feed"
//         component={Feed}
//         options={{
//           tabBarLabel: 'Home',
//           tabBarIcon: ({ color }) => (
//             <MaterialCommunityIcons name="home" color={color} size={27} />
//           ),
//         }}
//       />
//       <Tab.Screen
//         name="Notifications"
//         component={Notifications}
//         options={{
//           tabBarLabel: 'Updates',
//           tabBarIcon: ({ color }) => (
//             <MaterialCommunityIcons name="bell" color={color} size={26} />
//           ),
//         }}
//       />
//       <Tab.Screen
//         name="Profile"
//         component={Profile}
//         options={{
//           tabBarLabel: 'Profile',
//           tabBarIcon: ({ color }) => (
//             <MaterialCommunityIcons name="account" color={color} size={26} />
//           ),
//         }}
//       />
//     </Tab.Navigator>
//   );
// }

// export default function App() {
//   return (
//     <NavigationContainer>
//       <MyTabs />
//     </NavigationContainer>
//   );
// }

// import React from 'react';  
// import {StyleSheet, Text, View,Button} from 'react-native'; 
// import {Avatar} from 'react-native-paper'; 
// import { createBottomTabNavigator, createAppContainer} from 'react-navigation';  
// import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';  
// import Icon from 'react-native-vector-icons/Ionicons';  
// import {default as HomeScren} from './screen/HomePage';  
// import {default as CameraPage} from './screen/camera.page';  
// // class HomeScreen extends React.Component {  
// //   render() {  
// //     return (  
// //         <View style={styles.container}>  
// //           <Text>Home Screen</Text>  
// //         </View>  
// //     );  
// //   }  
// // }  
// // class CameraScreen extends React.Component {  
// //   render() {  
// //     return (  
// //         <View style={styles.container}>  
// //           <Text>Profile Screen</Text>  
// //         </View>  
// //     );  
// //   }  
// // }  
// class ImageScreen extends React.Component {  
//     render() {  
//         return (  
//             <View style={styles.container}>  
//                 <Text>Image Screen</Text>  
//             </View>  
//         );  
//     }  
// }  
// class CartScreen extends React.Component {  
//     render() {  
//         return (  
//             <View style={styles.container}>  
//                 <Text>Cart Screen</Text>  
//             </View>  
//         );  
//     }  
// }  
// const styles = StyleSheet.create({  
//     container: {  
//         flex: 1,  
//         justifyContent: 'center',  
//         alignItems: 'center'  
//     },  
// });  
// const TabNavigator = createMaterialBottomTabNavigator(  
//     {  
//         Home: { screen: HomeScren,  
//             navigationOptions:{  
//                 tabBarLabel:'Home',  
//                 tabBarIcon: ({ tintColor }) => (  
//                     <View>  
//                         {/* <Icon style={[{color: tintColor}]} size={25} name={'ios-home'}/>   */}
//                         <Avatar.Image  size={20} source={require('./image/start_hightlight.png')} />
//                     </View>),  
//             }  
//         },  
//         Camera: { screen: CameraPage,  
//             navigationOptions:{  
//                 tabBarLabel:'Profile',  
//                 tabBarIcon: ({ tintColor }) => (  
//                     <View>  
//                         {/* <Icon style={[{color: tintColor}]} size={25} name={'ios-person'}/>   */}
//                         <Avatar.Image  size={20} source={require('./image/start_hightlight.png')} />
//                     </View>),  
//                 activeColor: '#f0edf6',  
//                 inactiveColor: '#226557',  
//                 barStyle: { backgroundColor: '#3BAD87' },  
//             }  
//         },  
//         Image: { screen: ImageScreen,  
//             navigationOptions:{  
//                 tabBarLabel:'History',  
//                 tabBarIcon: ({ tintColor }) => (  
//                     <View>  
//                         <Avatar.Image  size={20} source={require('./image/start_hightlight.png')} />  
//                     </View>),  
//                 activeColor: '#f0edf6',  
//                 inactiveColor: '#226557',  
//                 barStyle: { backgroundColor: '#3BAD87' },
//             }  
//         },  
//         Cart: {  
//             screen: CartScreen,  
//             navigationOptions:{  
//                 tabBarLabel:'Cart',  
//                 tabBarIcon: ({ tintColor }) => (  
//                     <View>  
//                         <Avatar.Image size={20} source={require('./image/start_hightlight.png')} />  
//                     </View>),  
//             }  
//         },  
//     },  
//     {  
//       initialRouteName: "Home",  
//       activeColor: '#f0edf6',  
//       inactiveColor: '#226557',  
//       barStyle: { backgroundColor: '#3BAD87' },  
//     },  
// );  
  
// export default createAppContainer(TabNavigator);  