import React from 'react';
import {
  createStackNavigator,
    createAppContainer,
    createSwitchNavigator,
  } from 'react-navigation';

import LoginScreen from '../screen/main';
import HomePage from '../screen/HomePage';
import DetailPage from '../screen/DetailPage';
import ForgotPage from '../screen/ForgotPage';
import CountryPage from '../screen/country';
import RegistrationPage from '../screen/RegistrationPage';
import RNCameraPage from '../screen/camera.page';
import Cat from '../screen/newSignupScreen';
import AppTabBar from '../screen/AppTabBar';

const AuthStack = createStackNavigator({ SignIn: LoginScreen });

export default createAppContainer(
    createSwitchNavigator({
        Main: AuthStack,
        Home: HomePage,
        Detail: DetailPage,
        Forgot: ForgotPage,
        Country: CountryPage,
        Camera: RNCameraPage,
        Signup: RegistrationPage,
        Login: Cat,
        Tab: AppTabBar,
      },
      {
        initialRouteName: 'Login',
      })
  );