import React from 'react';  
import {StyleSheet, Text, View,Button} from 'react-native'; 
import {Avatar} from 'react-native-paper'; 
import { createBottomTabNavigator, createAppContainer} from 'react-navigation';  
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';  
import Icon from 'react-native-vector-icons/Ionicons';  
import {default as HomeScren} from '../screen/HomePage';  
import {default as CameraPage} from '../screen/camera.page';  
// class HomeScreen extends React.Component {  
//   render() {  
//     return (  
//         <View style={styles.container}>  
//           <Text>Home Screen</Text>  
//         </View>  
//     );  
//   }  
// }  
// class CameraScreen extends React.Component {  
//   render() {  
//     return (  
//         <View style={styles.container}>  
//           <Text>Profile Screen</Text>  
//         </View>  
//     );  
//   }  
// }  
class ImageScreen extends React.Component {  
    render() {  
        return (  
            <View style={styles.container}>  
                <Text>Image Screen</Text>  
            </View>  
        );  
    }  
}  
class CartScreen extends React.Component {  
    render() {  
        return (  
            <View style={styles.container}>  
                <Text>Cart Screen</Text>  
            </View>  
        );  
    }  
}  
const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        justifyContent: 'center',  
        alignItems: 'center'  
    },  
});  
const TabNavigator = createMaterialBottomTabNavigator(  
    {  
        Home: { screen: HomeScren,  
            navigationOptions:{  
                tabBarLabel:'Home',  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        {/* <Icon style={[{color: tintColor}]} size={25} name={'ios-home'}/>   */}
                        <Avatar.Image  size={20} source={require('../image/start_hightlight.png')} />
                    </View>),  
            }  
        },  
        Camera: { screen: CameraPage,  
            navigationOptions:{  
                tabBarLabel:'Profile',  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        {/* <Icon style={[{color: tintColor}]} size={25} name={'ios-person'}/>   */}
                        <Avatar.Image  size={20} source={require('../image/start_hightlight.png')} />
                    </View>),  
                activeColor: '#f0edf6',  
                inactiveColor: '#226557',  
                barStyle: { backgroundColor: '#3BAD87' },  
            }  
        },  
        Image: { screen: ImageScreen,  
            navigationOptions:{  
                tabBarLabel:'History',  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Avatar.Image  size={20} source={require('../image/start_hightlight.png')} />  
                    </View>),  
                activeColor: '#f0edf6',  
                inactiveColor: '#226557',  
                barStyle: { backgroundColor: '#3BAD87' },
            }  
        },  
        Cart: {  
            screen: CartScreen,  
            navigationOptions:{  
                tabBarLabel:'Cart',  
                tabBarIcon: ({ tintColor }) => (  
                    <View>  
                        <Avatar.Image size={20} source={require('../image/start_hightlight.png')} />  
                    </View>),  
            }  
        },  
    },  
    {  
      initialRouteName: "Home",  
      activeColor: '#f0edf6',  
      inactiveColor: '#226557',  
      barStyle: { backgroundColor: '#3BAD87' },  
    },  
);  
  
export default createAppContainer(TabNavigator);  