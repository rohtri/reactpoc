import React from "react";
import {
StyleSheet,
View,
ActivityIndicator,
FlatList,
Text,
SafeAreaView,
TouchableOpacity
} from "react-native";
import NavigationBar from 'react-native-navbar';

export default class SecondActivity extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
           data: this.props.navigation.getParam('data', {}) 
        }
        console.log(this.state);
       }
       static navigationOptions = ({ navigation }) => {
        };
        componentDidMount(){
        let obj = {};
        let country = this.state.data.item.countryCode;
        obj.query = `country.countryCode=='${country}'`;
        obj.sortBy = 'instituteName';
        obj.sortType = 'desc';
        obj.page = '-1';
        obj.limit = '-1';
        this.sendPost(obj);
        }
        sendPost(body) {
            let url = 'https://www.milliporesigmabioinfo.com/edge/institute/v1/search';
            fetch(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',},
              body: JSON.stringify(body)
            }).then(response => response.json())
            .then((responseJson)=> {
              this.setState({
               loading: false,
               dataSource: responseJson
              })
            })
            .catch(error=>console.log(error)); //to catch the errors if any
        }
            renderItem=(universityData)=>
<TouchableOpacity onPress={() => this.onItemPress(universityData)} style={[styles.item]}>
<Text style={styles.listTitle}>{universityData.item.key}</Text>
<Text style={styles.subTitle}>{universityData.item.value + ' Researchers'}</Text>
</TouchableOpacity>
 
  render()
  {
    if(this.state.loading){
        return( 
          <View style={styles.loader}> 
            <ActivityIndicator size="large" color="#0c9"/>
          </View>
      )}

     return(
<SafeAreaView style={styles.container}>
    <NavigationBar
    title={titleConfig}
    leftButton={ {
        title: 'Back',
        handler: () => this.props.navigation.navigate('Country'),
      }
    }
  />
 <View style={styles.container}>
 <FlatList
    data= {this.state.dataSource}
    ItemSeparatorComponent = {this.FlatListItemSeparator}
    renderItem= {item=> this.renderItem(item)}
    keyExtractor= {item=>item.affiliationsId.toString()}
 />
</View>
</SafeAreaView>
     );
  }
}
  const titleConfig = {
    title: 'Universities',
  };
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 20,
    backgroundColor: "#fff"
   },
  loader:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
   },
  list:{
    paddingVertical: 4,
    margin: 5,
    backgroundColor: "#fff"
   },
   item: {
    backgroundColor: '#E5E8E8',
    padding: 20,
    marginVertical: 10,
    marginHorizontal: 16,
  },
  listTitle: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  subTitle: {
    fontSize: 15,
    marginVertical: 5
  },
});