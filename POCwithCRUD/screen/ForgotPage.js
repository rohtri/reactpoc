import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import { Button, TextInput, withTheme, Avatar} from 'react-native-paper';

class ForgotPage extends React.Component {
    constructor() {
      super();
      this.state = {
        email: '',
        password: '',
        error: '',
        loading: false
      }
    }
     static navigationOptions = {
        title: 'Forgot Page'
    };
    render() {
        const inputStyle = { margin: 10 }
        return (
          <View style={styles.container} >
          <View style={styles.logoIcon} >
              <Avatar.Image  size={100} source={require('../assets/logo.png')} />
          </View>
             <View style={styles.containerDiv} >
                <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='email'
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                />
                {/* <Text style={styles.forgot}>Forgot Password ?</Text> */}
                <Text style={styles.error}>{!this.state.loading ? this.state.error : ''}</Text>
                <Button
                      disabled={this.state.loading}
                      loading={this.state.loading}
                      dark={true}
                      icon={'add'}
                      theme={{ roundness: 3 }}
                      mode="contained"
                      onPress={this._signInAsync}
                      style={inputStyle}
                    >Okay</Button>
                    {/* <Text style={styles.forgot}>Not a memeber? Register here</Text> */}
                </View>
              </View>
            );
      }
    
      _signInAsync = () => {
        this.props.navigation.navigate('Main');
      };
    }
    const styles = StyleSheet.create({
      container: {
        flex:1,
        justifyContent: "center"
      },
      forgot: {
        marginLeft: 15,
        color: '#007bff',
        justifyContent: "center"
      },
      containerDiv: {
        flex:2,
      },
      error: {
        color:"red",
        margin: 15
      },
      input: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        //width: DEVICE_WIDTH - 40,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 45,
        borderRadius: 20,
        marginBottom: 10,
        
      },
      logoIcon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
      },
      buttonlogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F035E0',
      },
    });
    
  export default withTheme(ForgotPage); 