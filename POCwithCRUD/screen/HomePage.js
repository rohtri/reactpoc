import React from 'react';
import { FlatList, StyleSheet, Text, SafeAreaView,
    TouchableOpacity, View } from 'react-native';
import NavigationBar from 'react-native-navbar';


const DATA = [
    {
      id: '1',
      title: 'First Item',
    },
    {
      id: '2',
      title: 'Second Item',
    },
    {
      id: '3',
      title: 'Third Item',
    },
    {
        id: '4',
        title: 'Fourth Item',
    },
    {
        id: '5',
        title: 'Fifth Item',
      },
  ];
  
  function Item({ id, title, selected, onSelect }) {
    return (
      <TouchableOpacity
        onPress={() => onSelect(id)}
        style={[
          styles.item,
          { backgroundColor: selected ? '#6e3b6e' : '#f9c2ff' },
        ]}
      >
        <Text style={styles.title}>{title}</Text>
      </TouchableOpacity>
    );
  }
export default FlatListBasics = ({ navigation }) => {
    const [selected, setSelected] = React.useState(new Map());

  const onSelect = React.useCallback(
    id => {
      const newSelected = new Map(selected);
      newSelected.set(id, !selected.get(id));

      setSelected(newSelected);
    },
    [selected],
  );

    return (
        <SafeAreaView style={styles.container}>
            <NavigationBar
        title={titleConfig}
        leftButton={leftButtonConfig}
        rightButton={rightButtonConfig}
      />
       <View style={styles.containerList} >
        <FlatList
          data={DATA}
          renderItem={({ item }) => (
            <Item
              id={item.id}
              title={item.title}
              selected={!!selected.get(item.id)}
              onSelect={onSelect}
            />
          )}
          keyExtractor={item => item.id}
          extraData={selected}
        />
        </View>
      </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      containerList: {
        flex: 1,
        backgroundColor: '#FCF3FD'
      },
      item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 10,
        marginHorizontal: 16,
      },
      title: {
        fontSize: 20,
      },
})
const rightButtonConfig = {
    title: 'Next',
    handler: () => alert('hello!'),
  };
  const leftButtonConfig = {
    title: 'Back',
    handler: () => alert('hello!'),
  };
  const titleConfig = {
    title: 'Welcome',
  };
 