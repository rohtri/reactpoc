import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
} from 'react-native';
import { Button, TextInput, withTheme, Avatar} from 'react-native-paper';
import NavigationBar from 'react-native-navbar';

class RegistrationPage extends React.Component {
    constructor() {
      super();
      this.state = {
          name: '',
          address:'',
          dob:'',
          phone:'',
        email: '',
        password: '',
        error: '',
        loading: false
      }
    }
     static navigationOptions = {
        title: 'Registration'
    };
    render() {
        const inputStyle = { margin: 10 }
        return (
          <SafeAreaView style={styles.container}>
          <NavigationBar
    title={titleConfig}
    leftButton={ {
        title: 'Back',
        handler: () => this.props.navigation.navigate('Main'),
      }
    }
  />
          <View style={styles.container}>
             <View style={styles.containerDiv}>
                <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='Full Name'
                  value={this.state.name}
                  onChangeText={name => this.setState({ name })}
                />
                 <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='Address'
                  value={this.state.address}
                  onChangeText={address => this.setState({ address })}
                />
                 <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='Phone'
                  value={this.state.phone}
                  onChangeText={phone => this.setState({ phone })}
                />
                 <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='DOB'
                  value={this.state.dob}
                  onChangeText={dob => this.setState({ dob })}
                />
                 <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='email'
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                />
                <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='Password'
                  value={this.state.password}
                  onChangeText={password => this.setState({ password })}
                />
                {/* <Text style={styles.forgot}>Forgot Password ?</Text> */}
                <Text style={styles.error}>{!this.state.loading ? this.state.error : ''}</Text>
                <Button
                      disabled={this.state.loading}
                      loading={this.state.loading}
                      dark={true}
                      icon={'add'}
                      theme={{ roundness: 3 }}
                      mode="contained"
                      onPress={this._signInAsync}
                      style={inputStyle}
                    >Sign Up</Button>
                    {/* <Text style={styles.forgot}>Not a memeber? Register here</Text> */}
                </View>
              </View>
              </SafeAreaView>
            );
      }
    
      _signInAsync = () => {
        this.props.navigation.navigate('Main');
      };
    }
    const titleConfig = {
      title: 'Registration',
    };
    const styles = StyleSheet.create({
      container: {
        flex:1,
        marginVertical: 20,
        justifyContent: "space-around"
      },
      forgot: {
        marginLeft: 15,
        color: '#007bff',
        justifyContent: "center"
      },
      containerDiv: {
        flex:2,
      },
      error: {
        color:"red",
        margin: 15
      },
      input: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        //width: DEVICE_WIDTH - 40,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 45,
        borderRadius: 20,
        marginBottom: 10,
        
      },
      logoIcon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
      },
      buttonlogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F035E0',
      },
    });
    
  export default withTheme(RegistrationPage); 