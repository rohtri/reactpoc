import React from "react";
import {
StyleSheet,
View,
ActivityIndicator,
FlatList,
Text,
SafeAreaView,
TouchableOpacity
} from "react-native";
import NavigationBar from 'react-native-navbar';
export default class Source extends React.Component {
static navigationOptions = ({ navigation }) => {
};
constructor(props) {
 super(props);
 this.state = {
   loading: true,
   dataSource:[]
  };
}
componentDidMount(){
fetch("https://www.milliporesigmabioinfo.com/edge/country/v1/countryList")
.then(response => response.json())
.then((responseJson)=> {
  this.setState({
   loading: false,
   dataSource: responseJson
  })
})
.catch(error=>console.log(error)) //to catch the errors if any
}
FlatListItemSeparator = () => {
return (
  <View style={{
     height: .5,
     width:"100%",
     backgroundColor:"rgba(0,0,0,0.5)",
}}
/>
);
}
onItemPress = (data) => {
   this.props.navigation.navigate('Detail', {data: data})
}
renderItem=(data)=>
<TouchableOpacity onPress={() => this.onItemPress(data)} style={[styles.item]}>
<Text style={styles.listTitle}>{data.item.countryName}</Text>
<Text style={styles.lightText}>{'Code- '+data.item.countryCode}</Text>
<Text style={styles.lightText}>{'currency- '+data.item.currency}</Text>
</TouchableOpacity>
render(){
 if(this.state.loading){
  return( 
    <View style={styles.loader}> 
      <ActivityIndicator size="large" color="#0c9"/>
    </View>
)}
return(
     <SafeAreaView style={styles.container}>
    <NavigationBar
    title={titleConfig}
    leftButton={ {
        title: 'Back',
        handler: () => this.props.navigation.navigate('Main'),
      }
    }
  />
 <View style={styles.container}>
 <FlatList
    data= {this.state.dataSource}
    ItemSeparatorComponent = {this.FlatListItemSeparator}
    renderItem= {item=> this.renderItem(item)}
    keyExtractor= {item=>item.countryId.toString()}
 />
</View>
</SafeAreaView>
)}
}

  const titleConfig = {
    title: 'Country',
  };
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 20,
    backgroundColor: "#fff"
   },
  loader:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
   },
  list:{
    paddingVertical: 4,
    margin: 5,
    backgroundColor: "#fff"
   },
   item: {
    backgroundColor: '#E5E8E8',
    padding: 20,
    marginVertical: 10,
    marginHorizontal: 16,
  },
  listTitle: {
    fontSize: 20,
  },
});