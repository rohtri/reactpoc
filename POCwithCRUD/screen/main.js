import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import { Button, TextInput, withTheme, Avatar} from 'react-native-paper';

class SignInScreen extends React.Component {
    constructor() {
      super();
      this.state = {
        email: 'rtrivedi@gmail.com',
        password: '1234',
        error: '',
        loading: false
      }
    }
     static navigationOptions = {
        title: 'Please sign in'
    };
    render() {
        const inputStyle = { margin: 10 }
        return (
          <View style={styles.container} >
          <View style={styles.logoIcon} >
          <Avatar.Image  size={100} source={require('../assets/logo.png')} />
          </View>
             
             <View style={styles.containerDiv} >
                <TextInput
                  dense={true}
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='email'
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                />
                <TextInput
                  error={false}
                  mode='outlined'
                  style={inputStyle}
                  placeholder='password'
                  value={this.state.password}
                  onChangeText={password => this.setState({ password })}
                />
                {/* <Text style={styles.forgot}>Forgot Password ?</Text> */}
                <Text style={styles.error}>{!this.state.loading ? this.state.error : ''}</Text>
                <Button
                      disabled={this.state.loading}
                      loading={this.state.loading}
                      dark={true}
                      icon={'add'}
                      theme={{ roundness: 3 }}
                      mode="contained"
                      onPress={this._signInAsync}
                      style={inputStyle}
                    >SIGN IN</Button>
                    <View style={styles.containerNewuser}>
                    <Text onPress={this._forgotAsync} style={styles.forgot}>Forget Password</Text>
                    <Text onPress={this._signupAsync} style={styles.newuser}>Not a memeber? Register here</Text>
                    </View>
                </View>
              </View>
            );
      }
      _forgotAsync = () => {
        this.props.navigation.navigate('Forgot');
      };
      _signupAsync = () => {
        this.props.navigation.navigate('Signup');
      };
      _signInAsync = () => {
      this.props.navigation.navigate('Country');
      };
    }
  
//   const DEVICE_WIDTH = Dimensions.get('window').width;
//   const DEVICE_HEIGHT = Dimensions.get('window').height;
//   const MARGIN = 40;
  
    const styles = StyleSheet.create({
      container: {
        flex:1,
        justifyContent: "center"
      },
      containerNewuser: {
        flex:1,
        flexDirection: "row",
        justifyContent: "space-around"
      },
      forgot: {
        marginLeft: 5,
        color: '#007bff',
        
      },
      newuser: {
        marginRight:5,
        color: '#007bff',
        
      },
      containerDiv: {
        flex:2,
      },
      error: {
        color:"red",
        margin: 15
      },
      input: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        //width: DEVICE_WIDTH - 40,
        height: 40,
        marginHorizontal: 20,
        paddingLeft: 45,
        borderRadius: 20,
        marginBottom: 10,
        
      },
      logoIcon: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
      },
      buttonlogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F035E0',
      },
    });
    
  export default withTheme(SignInScreen);    
