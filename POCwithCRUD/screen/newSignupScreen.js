import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import { Button, TextInput, withTheme, Avatar} from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';

export default class Cat extends React.Component {
    render() {
      return (
        <View style={styles.mainContainer} >
        <View style={styles.headerContainer}>
        <Text style={styles.headerTextStyle}>Supelco</Text>
        <Text style={styles.headerTextStyle}>M</Text>
        </View>
        <Text style={styles.whiteheaderTextStyle}>MQuant® StripScan</Text>
        <View style={styles.layoutContainer}>
            <Text style={styles.welcomeTextStyle}>Welcome!</Text>
            <View style={styles.loginContainer}>
            <View>
            <Text style={styles.emailpassTextStyle}>E-Mail</Text>
            <TextInput style={styles.placeholderStyle} placeholder='Please insert your e-mail' underlineColor='#128D51'>
            </TextInput>
            </View>
            <View>
            <Text style={styles.emailpassTextStyle}>Password</Text>
            <TextInput textContentType='password' style={styles.placeholderStyle} placeholder='Please insert your password' underlineColor='#128D51'>
            </TextInput>
            </View> 
            <View style={{flexDirection:'row-reverse', padding:10}} >
            <Text style={styles.forgot}>Forget Password?</Text>
            </View>
            <View style={{alignItems:'center'}}>
            <Button style={styles.loginButtonStyle} color='#FFFFFF' onPress={this._signInAsync}>Login</Button>
            <Button color='#128D51'>SignUp</Button>
            <Button color='#128D51' fontWeight='bold'>Continue as Guest</Button>
            </View>
            </View>
        </View>
        </View>
      );
    }
    _signInAsync = () => {
      this.props.navigation.navigate('Tab');
      };
  }
  const styles = StyleSheet.create({
    mainContainer: {
      backgroundColor : '#128D51',
      flex:1,
      justifyContent: "flex-start",
      paddingTop: 40
    },
    headerContainer:{
        flex:0.5,
        flexDirection:'row',
        justifyContent:'space-between',
        padding:5,
        backgroundColor:"#128D51",
    },
    layoutContainer: {
        backgroundColor : '#FFFFFF',
        flex:8,
        margin:12,
        borderRadius:20,
        justifyContent: 'space-evenly',
        paddingTop: 20
      },
      loginContainer:{
        flex:1,
        paddingTop:25,
        paddingLeft:5,
        justifyContent:"space-evenly"
      },
    headerTextStyle: {
        color : '#FECA31',
        fontSize : 25,
        fontWeight: 'bold',
        padding: 10,
        fontFamily:'AvenirNext-Medium'
      },
      whiteheaderTextStyle:{
          color: '#FFFFFF',
          fontSize:25,
          padding:15,
          fontWeight:"bold",
          justifyContent:'flex-start'
      },
      welcomeTextStyle: {
        color : "black",
        fontSize : 35,
        fontWeight: 'bold',
        paddingLeft:10
      },
      emailpassTextStyle: {
        color : "gray",
        fontSize : 16,
        paddingLeft:10
      },
      placeholderStyle:{
          backgroundColor:'#EFF0EF',
          margin:10,
          borderRadius:10,
      },
      loginButtonStyle:{
          padding:10,
          fontSize:20,
          paddingLeft:20,
          paddingRight:20,
          textDecorationColor:'#FFFFFF',
          backgroundColor:'#128D51',
          borderRadius:10
      }
    });